# Bootstrap 4 Template

Bootstrap 4 Starter Template using Gulp and Node.js.

# Quick Overview
- Download or clone this repository to your development environment.
- Install Compass on your computer [Read Compass documentation here http://compass-style.org/help].
- Open command line and change directory to the project folder [cd bootstrap4-theme].
- Run compass [compass watch].

```sh
cd bootstrap4-theme
compass watch
```

# Demo

http://www.rascojet.com/github/bootstrap4-theme

## License

[MIT](http://opensource.org/licenses/MIT)
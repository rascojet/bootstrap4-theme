// Demo Scripts

$(document).ready(function() {
	$('[data-style]').on('click', function(){
		$this = $(this);
		$('link').each(function() {
			if ($(this).attr('class') == 'default-color') {
				$(this).attr('href', $this.data('url'));      
			}
		});
	});
	
});	

const toggle = document.querySelector('#change-theme');
const theme = document.querySelector('.default-color');
toggle.addEventListener('change', (e) => {
  const isDark = e.target.checked;
  theme.setAttribute('href', isDark ? 'css/theme-dark.css' : 'css/theme-light.css');
})